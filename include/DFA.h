#ifndef DFA_H
#define DFA_H

#include "main.h"

void DFA_Init(char *szArquivo);
void DFA_Estado1(char caracter);
void DFA_Estado2(char caracter);
void DFA_Estado3(char caracter);
void DFA_Estado4(char caracter);
void DFA_Estado5();

#endif