#ifndef _MAIN_H
#define _MAIN_H

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "DFA.h"

enum myBool{
    FALSE = 0,
    TRUE
};

typedef enum eEstados_t {
    estado_1 = 0,
    estado_2,
    estado_3,
    estado_4,
    estado_5
} eEstados;

void TableInit(char *szArquivo);

#endif