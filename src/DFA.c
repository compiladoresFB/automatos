#include "DFA.h"

unsigned int uiIndex;
eEstados estadoAtual;
enum myBool terminar;
enum myBool isError;

/* Automato para verificação de comentários com multiplas linhas em C */
void DFA_Init(char *szArquivo){
    uiIndex = 0;
    terminar = FALSE;
    isError = FALSE;
    estadoAtual = estado_1;

    while(TRUE){
        switch(estadoAtual){
            case estado_1:
                DFA_Estado1(szArquivo[uiIndex]);
                break;
            case estado_2:
                DFA_Estado2(szArquivo[uiIndex]);
                break;
            case estado_3:
                DFA_Estado3(szArquivo[uiIndex]);
                break;
            case estado_4:
                DFA_Estado4(szArquivo[uiIndex]);
                break;
            case estado_5:
                DFA_Estado5();
                break;
        }

        if(isError){
            printf("Erro de sintaxe no comentario\n");
            break;
        }

        if(terminar){
            printf("Comentario:\n%s\n", szArquivo);
            break;
        }
    }
}

void DFA_Estado1(char caracter){
    if(caracter == '/'){
        uiIndex++;
        estadoAtual = estado_2;
        return;
    }
    isError = TRUE;
}

void DFA_Estado2(char caracter){
    if(caracter == '*'){
        uiIndex++;
        estadoAtual = estado_3;
        return;
    }
    isError = TRUE;
}

void DFA_Estado3(char caracter){
    if(caracter == '\0'){
        isError = TRUE;
    }
    else if(caracter != '*'){
        uiIndex++;
    }
    else if(caracter == '*'){
        uiIndex++;
        estadoAtual = estado_4;
    }
}

void DFA_Estado4(char caracter){
    if(caracter == '\0'){
        isError = TRUE;
    }
    else if(caracter == '*'){
        uiIndex++;
    }
    else if(caracter == '/'){
        estadoAtual = estado_5;
    }
    else{
        uiIndex++;
        estadoAtual = estado_3;
    }
}

void DFA_Estado5(){
    terminar = TRUE;
}