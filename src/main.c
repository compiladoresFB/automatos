#include "main.h"

int main(int argc, char **argv){
    unsigned int uiFileSize;
    char chArquivo;
    char *szArquivo;

    // Verificando se foram passados os parametros certos
    if(argc != 3){
        printf("Usage: %s --automato/--tabela [comentario.txt]\n", argv[0]);
        return 1;
    }

    // Abrindo o arquivo e verificando se ele existe
    FILE *fdComentario = fopen(argv[2], "r");
    if(fdComentario == NULL){
        printf("Arquivo \"%s\" nao foi encontrado\n", argv[2]);
        return 1;
    }

    // Pegando o tamanho do arquivo
    fseek(fdComentario, 0L, SEEK_END);
    uiFileSize = ftell(fdComentario);
    rewind(fdComentario);

    szArquivo = malloc(uiFileSize+1);

    for(int i = 0;; i++){
        chArquivo = fgetc(fdComentario);
        if(chArquivo == EOF) {
            szArquivo[i] = '\0';
            break;
        }

        szArquivo[i] = chArquivo;
    }

    if(strcmp(argv[1], "--automato") == 0)
        DFA_Init(szArquivo);
    else if(strcmp(argv[1], "--tabela") == 0)
        TableInit(szArquivo);
    else{
        printf("Usage: %s --automato/--tabela [comentario.txt]\n", argv[0]);
        return 1;
    }

    fclose(fdComentario);
    free(szArquivo);
    return 0;
}